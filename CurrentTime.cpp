#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <thread>
#include <chrono>
#include <functional>
#include <mutex>

using namespace std;

// For Task 1:
void print_time()
{
	const auto now = chrono::system_clock::now();
	const time_t t_c = chrono::system_clock::to_time_t(now);
	cout << "Task 1:" << ctime(&t_c);
}

// For Task 2:
class background_task {
public:
	void operator()() const
	{
		time_t result = time(nullptr);
		cout << "Task 2:" << asctime(localtime(&result));
	}
};


// Recursion for Task 4:
void create_thread(int threads_numb)
{
	if ((threads_numb) > 0)
	{
		auto id = this_thread::get_id(); // For debugging
		cout << this_thread::get_id() << endl;
		thread* new_thread = new thread(create_thread, --threads_numb);
		new_thread->join();
		delete new_thread;
	}
}

// For Task 5:
int g_number = 0;
mutex g_mtx;
void print_odd_numbers();

void print_even_numbers()
{
	thread t_odd(print_odd_numbers); // Odd thread
	{
		while (true)
		{
			{
				lock_guard<mutex> guard(g_mtx);
				if (g_number > 10)
					break;
				if (0 == g_number % 2)
				{
//					cout << oslock << "Thread 1: " << g_number++ << endl << osunlock;
					cout << "Thread 1: " << g_number++ << endl;
				}
			}
		}
	}
	t_odd.join();
}

void print_odd_numbers()
{
	{
		while (true)
		{
			{
				lock_guard<mutex> guard(g_mtx);
				if (g_number > 9)
					break;
				if (1 == g_number % 2)
				{
//					cout << oslock << "Thread 2: " << g_number++ << endl << osunlock;
					cout << "Thread 2: " << g_number++ << endl;
				}

			}

		}
	}
}

// For Task 6:
const int iThreadsNum = 6, iThreadSize = 50, iArrSize = iThreadsNum * iThreadSize;
int A[iArrSize][iArrSize], B[iArrSize][iArrSize], C[iArrSize][iArrSize];

void mult_matrix_part(int beg, int end, int a[iArrSize][iArrSize], int b[iArrSize][iArrSize], int (&c)[iArrSize][iArrSize])
{
	int i, j, k, sum;
	for(i = 0; i < iArrSize; ++i)
		for (j = beg; j < end; ++j)
		{
			sum = 0;
			for (k = 0; k < iArrSize; ++k)
			{
				sum += a[i][k] * b[k][j];
			}
			c[i][j] = sum;
		}
}



int main()
{
// Task 1:
	thread t;
	t = thread(print_time);
	t.join();

// Task 2:
	background_task bgtask;
	thread mythread(bgtask); // Use functor
// Wait in main thread:
	mythread.join();

// Task 3:
	chrono::system_clock::time_point start;
	thread mythreadlambda([&start]() -> void {
// Read from the thread current time into external variable t_c:
		start = chrono::system_clock::now();

		const time_t t_c_start = chrono::system_clock::to_time_t(start);
		cout << "Task 3:" << ctime(&t_c_start);
		});
	mythreadlambda.join();
	const auto end = chrono::system_clock::now();
//	auto t_c_end = chrono::system_clock::to_time_t(end);
	typedef chrono::duration<float> fsec;
	fsec fs = end - start;
	auto elapsed = (chrono::duration_cast<chrono::microseconds>)(fs);
	cout << "Task 3: elapsed time in microseconds: " << elapsed.count() << endl;

// Task 4 (recursion):
	auto threads_numb = thread::hardware_concurrency();
// function<void(int)> tlambda
	if (threads_numb > 0)
	{
		thread threadlambda(create_thread, threads_numb);
		threadlambda.join();
	}

// Task 5:
	g_number = 0;
	thread t1(print_even_numbers);  // Even
	t1.join();

// Tasks 6 and 7:
//	const int iThreadsNum = 6, iThreadSize = 50, iArrSize = iThreadsNum * iThreadSize;
	int iVal = 0;
// Initialization of A & B:
	for(int i=0; i<iArrSize; ++i)
	{
		for (int j = 0; j <iArrSize; j++)
		{
			A[i][j] = iVal++;
			B[i][j] = iVal++;
		}
	}

// Multithreads mode:
	thread threads[iThreadsNum]; // declare array of empty thread handles
// Spawn threads
	int iBeg = 0;
	const auto start_m = chrono::system_clock::now();
	for (size_t i = 0; i < iThreadsNum; i++)
	{
		threads[i] = thread(mult_matrix_part, iBeg, (iBeg + iThreadSize), A, B, ref(C));
		iBeg += iThreadSize;
	}
// Wait for threads
	for (size_t i = 0; i < iThreadsNum; i++)
	{
		threads[i].join();
	}
	const auto end_m = chrono::system_clock::now();

	fs = end_m - start_m;
	auto elapsed_m = (chrono::duration_cast<chrono::microseconds>)(fs);
	cout << "Task 6, multithreads mode: elapsed time in microseconds: " << elapsed_m.count() << endl;

// Single thread mode
	const auto start_s = chrono::system_clock::now();
	mult_matrix_part(0, iArrSize, A, B, ref(C));
	const auto end_s = chrono::system_clock::now();

	fs = end_s - start_s;
	auto elapsed_s = (chrono::duration_cast<chrono::microseconds>)(fs);
	cout << "Task 6, singlethread mode: elapsed time in microseconds: " << elapsed_s.count() << endl;

	cout << "Conclusion: there is an acceleration, but not 6 times, since the losses occur due to the need to create and switch threads, wait for them to end, etc.";

	return 0;
}